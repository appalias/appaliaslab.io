# App Alias Homepage Deployed

[![Pipeline](https://gitlab.com/appalias/appalias.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/appalias/appalias.gitlab.io/pipelines)

This repository holds the built static website for the appalias homepage.
Do not work with this repo directly.
Please work with the [website-www](https://gitlab.com/appalias/website-www) repo which has deployment information.
This repo is can be viewed without any authentication, please leave out any sensitive information.

You should be able to view this website live at [appalias.gitlab.io](https://appalias.gitlab.io) or [www.appalias.com](https://www.appalias.com/).
